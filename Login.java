
package healthtracker;

import java.io.*;
import java.util.Scanner;

/**
 *
 * @author Harshil Patel
 */

public class Login {

    public final String username;
    private final int passwordHash;

    public Login(String user, String pass) {
        username = user;
        passwordHash = pass.hashCode();

    }

    public String getUsername()
    {
        return this.username;
    }

    public boolean isUser() {
        Scanner scanner = null;

        try {
            //uses try catch exception to read in a file
            scanner = new Scanner(new File("users.txt"));
        } catch (FileNotFoundException e) {
            //if the text file cannot be found the following message is outputted
            System.err.println("Could not find file");
        }
        int count = 0;
        //while there is still a next line in the file read in the following will be run
        while (scanner.hasNext()) {
            System.out.println(count);
            /*
              the whole line is stored as a string, the string is then broken
              down into parts seperated by '#', the parts are then stored into
              an array
             */
            String line = scanner.nextLine();
            String[] users = line.split("#next#");

            for (int i = 0; i < users.length; i++) {
                //for every item in the 'parts' array, the item is then split again by each ','
                String[] parts = users[i].split(",");
                //the parts are then assign to the given attributes
                String nextUser = parts[2];
                int nextPass = Integer.parseInt(parts[3]);

                if (username.equals(nextUser) && passwordHash == nextPass) {
                    return true;
                }

            }
        }
        return false;
    }

    //returns true if recovery email was sent
    public static boolean recoverAccount(String userEmail, String newPass) throws IOException {
        boolean accountFound = false;
        Scanner scanner = null;
        String newUserContent = "";

        try {
            //uses try catch exception to read in a file
            scanner = new Scanner(new File("users.txt"));
        } catch (FileNotFoundException e) {
            //if the text file cannot be found the following message is outputted
            System.err.println("Could not find file");
        }

        //while there is still a next line in the file read in the following will be run
        while (scanner.hasNext()) {

            String line = scanner.nextLine();
            String[] users = line.split("#next#");

            for (int i = 0; i < users.length; i++) {

                String[] parts = users[i].split(",");

                String nextEmail = parts[4];

                if (userEmail.equals(nextEmail)) {
                    String editedUser = parts[0] + "," + parts[1] + "," + parts[2] + "," + newPass.hashCode()
                            + "," + parts[4] + "#next#";
                    newUserContent = newUserContent + editedUser;
                    //full website version will send password reset link to users email
                    accountFound = true;
                } else {
                    newUserContent = newUserContent + users[i] + "#next#";
                }

            }
        }

        if (accountFound) {
            Writer fileWriter = new FileWriter("users.txt");

            fileWriter.write(newUserContent);
            fileWriter.close();
        }

        return accountFound;
    }

    public static void main(String args[]) throws IOException {
        Login t = new Login("test", "test12");
        if (t.isUser()) {
            System.out.println("Success");
        } else {
            System.out.println("INCORRECT DETAILS");
        }

        //if (Login.recoverAccount("test@gmail.com", "test12")) {
        //    System.out.println("Account found and password changed");
        //} else {
        //    System.out.println("No account with that email");
        //}

    }

}
