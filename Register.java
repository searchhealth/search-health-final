
package healthtracker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Harshil Patel
 */

public class Register {

    String firstname;
    String surname;
    String username;
    int passwordHash;
    String email;

    public Register(String fName, String lName, String user, String pass,
                         String email) {

        firstname = fName;
        surname = lName;
        username = user;
        passwordHash = pass.hashCode();
        this.email = email;
    }

    public boolean registerUser() throws FileNotFoundException, IOException {
        if (!areDetailsTaken()) {
            if (isValidEmail()) {

                String userDetails = firstname + "," + surname + "," + username + ","
                        +passwordHash+ "," + email + "#next#";

                FileOutputStream outputStream = new FileOutputStream("users.txt", true);
                byte[] strToBytes = userDetails.getBytes();
                outputStream.write(strToBytes);

                outputStream.close();
                return true;

            }
            else
            {
                System.out.println("Invalid email");
            }
        }
        System.out.println("Details are taken");
        return false;
    }

    public boolean areDetailsTaken() {
        Scanner scanner = null;

        try {
            //uses try catch exception to read in a file
            scanner = new Scanner(new File("users.txt"));
            System.out.println("test");
        } catch (FileNotFoundException e) {
            //if the text file cannot be found the following message is outputted
            System.err.println("Could not find file");
        }
        int count = 0;
        //while there is still a next line in the file read in the following will be run
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            String[] users = line.split("#next#");

            for (int i = 0; i < users.length; i++) {
                count++;
                System.out.println(count);
                String[] parts = users[i].split(",");

                String nextUser = parts[2];
                String nextEmail = parts[4];

                if (username.equals(nextUser) || email.equals(nextEmail)) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean isValidEmail() {

        String emailRegex = "^(.+)@(.+)$";
        Pattern pattern = Pattern.compile(emailRegex);

        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }

    public static void main(String args[]) throws IOException {
        Register t = new Register("test1", "test1", "test1", "test1", "test1@gmail.com");
        if(t.registerUser()){
            System.out.println("Success");
        } else {
            System.out.println("Invalid entry");
        }

    }

}
